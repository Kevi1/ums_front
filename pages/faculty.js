import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { useGetAllFacultysQuery,useDeleteFacultyMutation } from '../src/reducers/facultyApi';
import { useEffect,useState } from 'react';
import FacultyCreate from '../src/modals/facultyCreate';
import FacultyEdit from '../src/modals/facultyEdit';
const { forwardRef, useRef, useImperativeHandle } = React;

const columns = [

  
  { id: 'name', label: 'Name', minWidth: 170 },
  
];


function createData(name,remove) {
  // const density = population / size;
  return { name,remove};
}


function Faculty() {
  const { data, error, isLoading,refetch} = useGetAllFacultysQuery()

  const [currentEdit,setCurrentEdit] = useState({})

  const [modalVisibility,setModalVisibility] = useState(false)

  const childRef = useRef();
  
  const [idToChange, setIdToChange] = useState(0)
  //   createData('China', 'CN', 1403500365, 9596961),
  
  const [
    deleteFaculty, // This is the mutation trigger
    { isLoading: isUpdating }, // This is the destructured mutation result
  ] = useDeleteFacultyMutation()

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const toggleModal = () =>{
    setModalVisibility(!modalVisibility)
  }
if(isLoading){
  return(<h1>LOADING</h1>)
}


const rows = data.map(
      (el)=> createData(el.name,el.id))


  return (
    <>

    <div style={{display:'flex',justifyContent:'center'}}>
      
    <FacultyCreate reload = {refetch}/>
    <FacultyEdit reload = {refetch} ref ={childRef} data={currentEdit} visible = {modalVisibility} changeModalVisibility={toggleModal}/>

    </div>
    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
          
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow onClick={() => {
                    setCurrentEdit(row)
                    // setIdToChange(row.remove)
                    toggleModal()}} hover role="checkbox" tabIndex={-1} key={row.remove}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell  key={column.id} align={column.align}>
                          {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value}
                        </TableCell>
                      );
                    })}
                          <TableCell
                  key={"remove"}
                  onClick={()=>{
                    deleteFaculty(row["remove"])
                  }}
                //   align={column.align}
                //   style={{ minWidth: column.minWidth }}
                >
                  
                  X
                </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
    </>
  );
}

export default Faculty