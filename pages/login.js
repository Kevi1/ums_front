import Box from "@mui/material/Box";
import React, { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { FormInputText } from "../form-components/FormInputText";
import { Grid, Button } from "@mui/material";
import Head from "next/head";
import { TfiEmail } from "react-icons/tfi";
import { CiLock } from "react-icons/ci";
import { useSelector, useDispatch } from "react-redux";
import { getTokens } from "../src/reducers/loginDataSlice";
import { useRouter } from "next/router";
import { getUserData } from "../src/reducers/userDataSlice";


// import validate from "@react-email-validator"
const validateEmail = (email) => {
  return email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
};

export default function Login() {
  const router = useRouter();
  const [loading,setLoading] = useState(false)
  const dispatch = useDispatch();
  const defaultValues = {
    username: "",
    password: "",
  };

  const methods = useForm({ defaultValues: defaultValues });
  const { handleSubmit, reset, control, setValue } = methods;
  const onSubmit = (data) => {
    setLoading(true)
    const loginSuccessful = dispatch(getTokens(data.username, data.password));
    loginSuccessful
      .then(async (data) => {
        if (data.payload) {
          console.log("WORKED")
         await dispatch(getUserData())
          router.push("/");
        }
      })
      .catch();
  };


  return (
    <div>
      <Head>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Tiro+Tamil&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: "100vh" }}
      >
        <div style={{ fontFamily: "Tiro Tamil", fontSize: "30px" }}>
          Log in:
        </div>
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "35ch" },
            display: "flex",
            "flex-direction": "column",
          }}
          noValidate
          autoComplete="off"
        >
          <FormInputText
            name="username"
            control={control}
            label="Username"
            Icon={<TfiEmail />}
            pattern={
              /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
            }
          />
          <FormInputText
            name="password"
            control={control}
            label="Password"
            text_type="password"
            Icon={<CiLock />}
            pattern={/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/}
          />
        </Box>
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "25ch" },
            display: "flex",
            "flex-direction": "column",
          }}
          noValidate
          autoComplete="off"
        >
          <Button
            sx={{ textTransform: "none", borderRadius: "29px" }}
            onClick={handleSubmit(onSubmit)}
            variant={"contained"}
          >
            Sign in
          </Button>
        </Box>
      </Grid>
    </div>
  );
}
