import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { useGetAllSubjectsQuery,useDeleteSubjectMutation } from '../src/reducers/subjectApi';
import { useEffect } from 'react';
import FacultyCreate from '../src/modals/facultyCreate';
const columns = [

  
  { id: 'title', label: 'Title', minWidth: 170 },
  { id: 'credits', label: 'Credits', minWidth: 170 },
  { id: 'code', label: 'Code', minWidth: 170 },
  { id: 'description', label: 'Description', minWidth: 170 },
];


function createData(title,credits,code, description, remove) {
  // const density = population / size;
  return {title,credits,code, description, remove};
}


export default function Subject() {
  const { data, error, isLoading,refetch} = useGetAllSubjectsQuery()

  const [
    deleteSubject, // This is the mutation trigger
    { isLoading: isUpdating }, // This is the destructured mutation result
  ] = useDeleteSubjectMutation()

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };


if(isLoading){
  return(<h1>LOADING</h1>)
}


const rows = data.map(
      (el)=> createData(el.title,el.credits,el.code,el.description,el.remove))


  return (
    <>

    <div style={{display:'flex',justifyContent:'center'}}>
    <FacultyCreate/>
    </div>
    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
          
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.remove}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value}
                        </TableCell>
                      );
                    })}
                          <TableCell
                  key={"remove"}
                  onClick={()=>{
                    deleteSubject(row["remove"])
                  }}
                //   align={column.align}
                //   style={{ minWidth: column.minWidth }}
                >
                  
                  X
                </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
    </>
  );
}