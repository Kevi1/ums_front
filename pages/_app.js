import * as React from 'react';
import Head from 'next/head';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider } from '@emotion/react';
import theme from '../config/theme';
import createEmotionCache from '../config/createEmotionCache';
import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'
import { persistor,store } from '../src/store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react';
import SearchAppBar from '../src/components/appbar'

config.autoAddCss = false
// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

const ContextA = React.createContext();

const App= ({ Component, props }) => {
  // const { store, props } = wrapper.useWrappedStore(rest)
  
  return (
    <>
    <CacheProvider value={clientSideEmotionCache}>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Provider store={store}>
          <PersistGate persistor={persistor} loading={null}>
            <SearchAppBar>

            </SearchAppBar>
        <Component {...props} />
      </PersistGate>

      </Provider>
      </ThemeProvider>
    </CacheProvider>
    </>
  );
}

export default App;