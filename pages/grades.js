import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { useGetAllDegreesQuery,useDeleteDegreeMutation } from '../src/reducers/degreeApi';
import { useEffect } from 'react';
import FacultyCreate from '../src/modals/facultyCreate';
const columns = [

  
  { id: 'name', label: 'Name', minWidth: 170 },
  { id: 'year', label: 'Year', minWidth: 170 },
  { id: 'type', label: 'Type', minWidth: 170 },
  { id: 'faculty_name', label: 'Faculty', minWidth: 170 },
  
  // {}
  // { id: 'code', label: 'ISO\u00a0Code', minWidth: 100 },
  // {
  //   id: 'population',
  //   label: 'Population',
  //   minWidth: 170,
  //   align: 'right',
  //   format: (value) => value.toLocaleString('en-US'),
  // },
  // {
  //   id: 'size',
  //   label: 'Size\u00a0(km\u00b2)',
  //   minWidth: 170,
  //   align: 'right',
  //   format: (value) => value.toLocaleString('en-US'),
  // },
  // {
  //   id: 'density',
  //   label: 'Density',
  //   minWidth: 170,
  //   align: 'right',
  //   format: (value) => value.toFixed(2),
  // },
  // {
  //   id:'remove',
  //   label:''
  // },
];


function createData(name,year,type,faculty_name,degree,remove) {
  // const density = population / size;
  return {name,year,type,faculty_name,degree,remove};
}


export default function Degree() {
  const { data, error, isLoading,refetch} = useGetAllDegreesQuery()

  //   createData('China', 'CN', 1403500365, 9596961),
  
  const [
    deleteDegree, // This is the mutation trigger
    { isLoading: isUpdating }, // This is the destructured mutation result
  ] = useDeleteDegreeMutation()

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };


if(isLoading){
  return(<h1>LOADING</h1>)
}


const rows = data.map(
      (el)=> createData(el.name,el.year,el.type,el.faculty_name,el.degree,el.id))


  return (
    <>

    <div style={{display:'flex',justifyContent:'center'}}>
    <FacultyCreate/>
    </div>
    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
          
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.remove}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value}
                        </TableCell>
                      );
                    })}
                          <TableCell
                  key={"remove"}
                  onClick={()=>{
                    deleteDegree(row["remove"])
                  }}
                //   align={column.align}
                //   style={{ minWidth: column.minWidth }}
                >
                  
                  X
                </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
    </>
  );
}