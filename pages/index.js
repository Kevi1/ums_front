import React, { useState } from "react";
import Box from '@mui/material/Box';
// import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Faculty from "./faculty";
import { Container } from "@mui/system";
import Degree from "./degree";
import Subject from "./subject";
import { withAuth } from '../src/utils'
import { withRedux } from 'next-redux-wrapper';
import { wrapper } from "../src/store";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import AuthGuard from "../src/components/AuthGuard";
// import store from './store'
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Container sx={{ p: 3,}}>
          {children}
        </Container>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

function Index() {
  const [value, setValue] = React.useState(0);
  const date = useSelector(state=>state)
  console.log(date)
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box
      sx={{ bgcolor: 'background.paper', display: 'flex', height: 300}}
    >
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: 'divider' }}
      >
        <Tab label="Facultys" {...a11yProps(0)} />
        <Tab label="Degrees" {...a11yProps(1)} />
        <Tab label="Subjects" {...a11yProps(2)} />

      </Tabs>
      <TabPanel value={value} index={0} style={{width:"100%"}}>
        <Faculty/>
      </TabPanel>
      <TabPanel value={value} index={1} style={{width:"100%"}}>
      
        <Degree/>
      </TabPanel>
      <TabPanel value={value} index={2}>
      <Subject/>

      </TabPanel>

    </Box>
  );
}

const ProtectedPageWrapper = () => (
  <AuthGuard role="asd">
    <Index />
  </AuthGuard>
)


export default ProtectedPageWrapper