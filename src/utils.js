import { useEffect } from 'react';
import Router from 'next/router'
import { fetchBaseQuery } from '@reduxjs/toolkit/dist/query';
// import { tokenReceived, loggedOut } from './authSlice'
import { Mutex } from 'async-mutex'
import { useDispatch } from 'react-redux';
import { refreshTokens } from './reducers/loginDataSlice';
import loginDataSlice from './reducers/loginDataSlice';

const withAuth = (WrappedComponent) => {
  const AuthenticatedComponent = (props) => {
    const {isLoggedIn} = props
    useEffect(() => {
      if (!isLoggedIn) {
        Router.push('/login')
      }
    }, [isLoggedIn])
    return <WrappedComponent {...props} />;
  };

  return AuthenticatedComponent;
};

// const makeQuery =fetchBaseQuery({
//    baseUrl: 'http://localhost:8000',
//    prepareHeaders: (headers, { getState }) => {
//     const token = getState().loginData.acessToken
//     // If we have a token set in state, let's assume that we should be passing it.
//     if (token) {
//       headers.set('authorization', `Bearer ${token}`)
//     }
//     return headers
//   }, })


  
  // create a new mutex
  const mutex = new Mutex()
  const baseQuery = fetchBaseQuery({ baseUrl: 'http://localhost:8000' , prepareHeaders: (headers, { getState }) => {
    const token = getState().loginData.acessToken
    console.log("ASDSAD")
    // If we have a token set in state, let's assume that we should be passing it.
    if (token) {
      headers.set('authorization', `Bearer ${token}`)
    }
    return headers
  }, })
  const baseQueryWithReauth = async (args, api, extraOptions) => {
    // wait until the mutex is available without locking it
    await mutex.waitForUnlock()
    let result = await baseQuery(args, api, extraOptions)
    console.log(result)
    if (result.error && result.error.status === 401) {
      // checking whether the mutex is locked
      if (!mutex.isLocked()) {
        const release = await mutex.acquire()
        try {
           const data = dispatch(refreshTokens())
          if (data) {
            // retry the initial query
            result = await baseQuery(args, api, extraOptions)
          } else {
            loginDataSlice.actions.logout()
          }
        } finally {
          // release must be called once the mutex should be released again.
          release()
        }
      } else {
        // wait until the mutex is available without locking it
        await mutex.waitForUnlock()
        result = await baseQuery(args, api, extraOptions)
      }
    }
    return result
  }


export {withAuth,baseQueryWithReauth};
