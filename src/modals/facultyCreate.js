import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { FormProvider, useForm } from "react-hook-form";
import { TfiEmail } from "react-icons/tfi";
import { CiLock } from "react-icons/ci";
import { FormInputText } from '../../form-components/FormInputText';
import {useAddNewFacultyMutation} from '../reducers/facultyApi'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};


    
export default function FacultyCreate(props) {
    const [
        creatFaculty, // This is the mutation trigger
        { isLoading: isUpdating }, // This is the destructured mutation result
      ] = useAddNewFacultyMutation()
const defaultValues = {
   name: "",
    
  };
  
  const methods = useForm({ defaultValues: defaultValues });
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  
  const { handleSubmit, reset, control, setValue } = methods;
  const onSubmit = (data) =>{
    creatFaculty(data)
    handleClose()
    props.reload()
}

  return (
    <div>
    <Button onClick={handleOpen}>Open modal</Button>
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          Text in a modal
        </Typography>
        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
        <Box
        component="form"
        sx={{
          '& > :not(style)': { m: 1, width: '35ch' },
          "display":"flex",
          "flex-direction": "column",
        }}
        noValidate
        autoComplete="off"
        >

          <FormInputText name="name" control={control} label="Name" Icon={<TfiEmail/>} pattern= {null}/>
       
            </Box>
    <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
        "display":"flex",
        "flex-direction": "column",
      
      }}
      noValidate
      autoComplete="off"
    >

      <Button sx = {{textTransform: "none" ,borderRadius:"29px"}} onClick={handleSubmit(onSubmit)} variant={"contained"}>
       Submit
      </Button>
      </Box>
        </Typography>
      </Box>
    </Modal>
  </div>
  );
}