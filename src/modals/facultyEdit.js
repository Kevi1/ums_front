import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { FormProvider, useForm } from "react-hook-form";
import { TfiEmail } from "react-icons/tfi";
import { CiLock } from "react-icons/ci";
import { FormInputText } from "../../form-components/FormInputText";
import {
  useAddNewFacultyMutation,
  useEditFacultyMutation,
} from "../reducers/facultyApi";
import { useState,useEffect,useMemo } from "react";
import { isEqual, uniq } from "lodash";

const { forwardRef, useRef, useImperativeHandle } = React;


const getUpdatedKeys = (oldData, newData) => {
  const data = uniq([...Object.keys(oldData), ...Object.keys(newData)]);
  const toReturn = {};
  for(const key of data){
    if(!isEqual(oldData[key], newData[key])){
      toReturn.key = newData[key];
    }
  }
  return toReturn;
}

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const FacultyEdit = forwardRef((props, ref) => {

    const [, updateState] = React.useState();

  const [
    editFaculty, // This is the mutation trigger
    { isLoading: isUpdating }, // This is the destructured mutation result
  ] = useEditFacultyMutation();


  const defaultValues = useMemo(() => {
    return props.data
  }, [props])


useEffect(() => {
  reset(props.data);
}, [props.data]);


  const methods = useForm({ defaultValues: defaultValues });
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => props.changeModalVisibility();

  if(props.visibility == true){
    handleOpen()
  }

  const { handleSubmit, reset, control, setValue } = methods;
  const onSubmit = (data) => {

    editFaculty(data,props.data.id);
    handleClose();
    // props.reload();
  };

  return (
    <div>
      <Button onClick={handleOpen}>Open modal</Button>
      <Modal
        open={props.visible}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Text in a modal
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }} component={'span'}>
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 1, width: "35ch" },
                display: "flex",
                "flex-direction": "column",
              }}
              noValidate
              autoComplete="off"
            >
              <FormInputText
                name="name"
                control={control}
                label="Name"
                Icon={<TfiEmail />}
                pattern={null}
              />
            </Box>
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 1, width: "25ch" },
                display: "flex",
                "flex-direction": "column",
              }}
              noValidate
              autoComplete="off"
            >
              <Button
                sx={{ textTransform: "none", borderRadius: "29px" }}
                onClick={handleSubmit(onSubmit)}
                variant={"contained"}
              >
                Submit
              </Button>
            </Box>
          </Typography>
        </Box>
      </Modal>
    </div>
  );
});
FacultyEdit.displayName = "FacultyEdit";
export default FacultyEdit;
