// import { applyMiddleware, createStore } from 'redux'
// import { composeWithDevTools } from 'redux-devtools-extension'
// import thunkMiddleware from 'redux-thunk'

// import monitorReducersEnhancer from './enhancers/monitorReducers'
// import loggerMiddleware from './middleware/logger'
// import rootReducer from './reducers'

// export default function configureStore(preloadedState) {
//   const middlewares = [loggerMiddleware, thunkMiddleware]
//   const middlewareEnhancer = applyMiddleware(...middlewares)

//   const enhancers = [middlewareEnhancer, monitorReducersEnhancer]
//   const composedEnhancers = composeWithDevTools(...enhancers)

//   const store = createStore(rootReducer, preloadedState, composedEnhancers)

//   if (process.env.NODE_ENV !== 'production' && module.hot) {
//     module.hot.accept('./reducers', () => store.replaceReducer(rootReducer))
//   }

//   return store
// }


import { configureStore } from '@reduxjs/toolkit'
import loginDataReducer from './reducers/loginDataSlice'
import userDataReducer from './reducers/userDataSlice'

import { facultyApi} from './reducers/facultyApi'
import { degreeApi } from './reducers/degreeApi'
import { subjectApi } from './reducers/subjectApi'
import { createWrapper } from 'next-redux-wrapper'
import { persistStore, persistReducer } from 'redux-persist'
import { combineReducers } from '@reduxjs/toolkit'
import thunk from 'redux-thunk'

import createWebStorage from "redux-persist/lib/storage/createWebStorage";

const createNoopStorage = () => {
  return {
    getItem(_key) {
      return Promise.resolve(null);
    },
    setItem(_key, value) {
      return Promise.resolve(value);
    },
    removeItem(_key) {
      return Promise.resolve();
    },
  };
};

const storage = typeof window !== "undefined" ? createWebStorage("local") : createNoopStorage();

export default storage;
const persistConfig = {
  timeout:"2000",
  key: 'root',
  storage,
} 
const rootReducer= combineReducers( {
  loginData: loginDataReducer,
  userData: userDataReducer,

  [facultyApi.reducerPath]: facultyApi.reducer,
  [degreeApi.reducerPath]: degreeApi.reducer,
  [subjectApi.reducerPath]: subjectApi.reducer,

},)

export const makeStore = () => configureStore({
  reducer: persistReducer(persistConfig, rootReducer),  
  middleware: [thunk,facultyApi.middleware ,degreeApi.middleware,subjectApi.middleware]
  // middleware: (getDefaultMiddleware) =>
  // getDefaultMiddleware().concat([facultyApi.middleware ,degreeApi.middleware,subjectApi.middleware])
})

const store = makeStore()
const persistor = persistStore(store);

// const wrapper = createWrapper(() => store,{debug:true})

export {persistor,store}