// First we need to import axios.js
import axios from 'axios';
import { store } from './store'

// Next we make an 'instance' of it


// console.log(store.getState())

// const api = axios.create({
// // .. where we make our configurations
//     baseURL: '127.0.0.1:8000'
// });


// axios.defaults.baseURL = '/api/'

// Where you would set stuff like your 'Authorization' header, etc ...

// instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

// Also add/ configure interceptors && all the other cool stuff


// export default api;