import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseQueryWithReauth } from '../utils'
// Define a service using a base URL and expected endpoints
export const facultyApi = createApi({
  reducerPath: 'facultyApi',
  baseQuery: baseQueryWithReauth,
  
  tagTypes:["Faculty"],
  endpoints: (builder) => ({
    getAllFacultys: builder.query({
      query: () => 'api/faculty',
      // headers: prepareHeaders,
      providesTags: ['Faculty']
    }),
    addNewFaculty: builder.mutation({
        query: (payload) => ({
          url: 'api/faculty/',
          method: 'POST',
          body: payload,
        }),
  }),
  editFaculty: builder.mutation({
    query: (payload,id) => ({
      url: `api/faculty/${payload.remove}/`,
      method: 'PUT',
      body: payload,
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }),
    invalidatesTags: ['Faculty'],

}),
  deleteFaculty: builder.mutation({
    query: (id) => ({
      url: `api/faculty/${id}`,
      method: 'DELETE',
      // credentials: 'include',
    }),
    invalidatesTags: ['Faculty'],
  })
    })
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetAllFacultysQuery,useDeleteFacultyMutation,useAddNewFacultyMutation,useEditFacultyMutation } = facultyApi