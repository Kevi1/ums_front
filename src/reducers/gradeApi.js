import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseQueryWithReauth } from '../utils'
// Define a service using a base URL and expected endpoints
export const gradeApi = createApi({
  reducerPath: 'gradeApi',
  baseQuery: baseQueryWithReauth,
  tagTypes:["grade"],
  endpoints: (builder) => ({
    getAllgrades: builder.query({
      query: (name) => 'api/grade',
      providesTags: ['grade']
    }),
    addNewGrade: builder.mutation({
        query: (payload) => ({
          url: 'api/Grade',
          method: 'POST',
          body: payload,
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        }),
  }),
  deleteGrade: builder.mutation({
    query: (id) => ({
      url: `api/grade/${id}`,
      method: 'DELETE',
      // credentials: 'include',
    }),
    invalidatesTags: ['Grade'],
  })
    })
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetAllGradesQuery,useDeleteGradeMutation } = gradeApi