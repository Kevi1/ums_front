import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { useSelector, useDispatch } from "react-redux";

// Define a service using a base URL and expected endpoints
export const subjectApi = createApi({
  reducerPath: 'subjectApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8000' }),
  tagTypes:["subject"],
  prepareHeaders: (headers, { getState }) => {
    const token = getState().loginData.acessToken
    // const token = useSelector((state) => state.loginData.accessToken);
    console.log(token)
    // If we have a token set in state, let's assume that we should be passing it.
    if (token) {
      headers.set('authorization', `Bearer ${token}`)
    }
    return headers
  },
  endpoints: (builder) => ({
    getAllSubjects: builder.query({
      query: (name) => 'api/subject',
      providesTags: ['Subject']
    }),
    addNewSubject: builder.mutation({
        query: (payload) => ({
          url: 'api/subject',
          method: 'POST',
          body: payload,
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        }),
  }),
  deleteSubject: builder.mutation({
    query: (id) => ({
      url: `api/subject/${id}`,
      method: 'DELETE',
      // credentials: 'include',
    }),
    invalidatesTags: ['Subject'],
  })
    })
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetAllSubjectsQuery,useDeleteSubjectMutation,useAddNewSubjectMutation } = subjectApi