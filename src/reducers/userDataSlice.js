import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import api from "../api";
import { useRouter } from "next/router";
import axios from "axios";
import { useSelector } from "react-redux";
const initialState = {
  username: "",
  role:"",
};
// const router = useRouter();

const getUserData = createAsyncThunk(
  "userData/getData",
  async (arg, { getState }) => {
    try {
      const state = getState()
    const acessToken = state.loginData.acessToken;

    const config = {
      headers: { Authorization: `Bearer ${acessToken}` }
  };    
      const data = {
        // username: "kevikostandini@gmail.com",
        // password: "Admin123456",
      };
      const response = await api.post("api/get_user_data",data, config);
      return response.data;
    } catch (error) {
      console.log(error);
    }
    return response.data;
  }
);

export const usernDataSlice = createSlice({
  name: "userData",
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(getUserData.pending, (state, action) => {
      // state.error = action.error.message
      state.error = "a";
      // console.log(action.payload)
    });

    builder.addCase(getUserData.fulfilled, (state, action) => {
      const data = action.payload;
      // Save the tokens in the state
      state.username = data.username;
      state.role = data.role;
      
    })
    builder.addCase(getUserData.rejected, (state, action) => {
      // state.error = action.error.message
      state.error = "c";
      // console.log(action.payload)
    });
  },
});

// Action creators are generated for each case reducer function
export { getUserData };


export default usernDataSlice.reducer;
