import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

// Define a service using a base URL and expected endpoints
export const degreeApi = createApi({
  reducerPath: 'degreeApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8000' }),
  tagTypes:["Degree"],
  endpoints: (builder) => ({
    getAllDegrees: builder.query({
      query: (name) => 'api/degree',
      providesTags: ['Degree']
    }),
    addNewDegree: builder.mutation({
        query: (payload) => ({
          url: 'api/degree',
          method: 'POST',
          body: payload,
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        }),
  }),
  deleteDegree: builder.mutation({
    query: (id) => ({
      url: `api/degree/${id}`,
      method: 'DELETE',
      // credentials: 'include',
    }),
    invalidatesTags: ['Degree'],
  })
    })
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetAllDegreesQuery,useDeleteDegreeMutation,useAddNewDegreeMutation } = degreeApi