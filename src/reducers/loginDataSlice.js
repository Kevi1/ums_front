import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import api from "../api";
import { useRouter } from "next/router";
import axios from "axios";
import { useDispatch } from "react-redux";
import { getUserData } from "./userDataSlice";
import { REHYDRATE } from 'redux-persist'

const initialState = {
  username: "",
  password: "",
  acessToken: "",
  refreshToken: "",
};

// const router = useRouter();

const getTokens = createAsyncThunk(
  "loginData/getTokens",
  async (email, pass) => {
    try {
      const data = {
        username: "kevikostandini@gmail.com",
        password: "Admin123456",
      };
      const response = await axios.post("http://127.0.0.1:8000/token/", data);
      return response.data;
    } catch (error) {
      console.log(error);
    }
    return response.data;
  }
);

const refreshTokens = createAsyncThunk(
  "loginData/refreshTokens",
  async (arg, { getState }) => { // <-- destructure getState method
    const state = getState(); // <-- invoke and access state object
    try {
      var config = {
        method: "post",
        url: '127.0.0.1:8000/token/refresh/',
        headers: {
          accept: "application/json",
        },
        body:{
          refresh:state.loginData.refreshToken
        }
      };

      const response = await axios(config);
      const data = await response.data;
      return data;
    } catch (error) {
      console.log(error);
    }
  }
);



export const loginDataSlice = createSlice({
  name: "loginData",
  initialState,
  reducers: {
    logout(state){
      state = initialState
    }
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed

    // });
    builder.addCase(getTokens.pending, (state, action) => {

      // state.error = action.error.message
      state.error = "a";
      // console.log(action.payload)
    });

    builder.addCase(getTokens.fulfilled, (state, action) => {
      // const dispatch = useDispatch();

      const data = action.payload;
      
      // Save the tokens in the state
      state.acessToken = data.access;
      state.refreshToken = data.refresh;
      // dispatch(getUserData())
      
      });
    builder.addCase(getTokens.rejected, (state, action) => {
      // state.error = action.error.message
      state.error = "c";
      // console.log(action.payload)
    });

    
    // });
    builder.addCase(refreshTokens.pending, (state, action) => {

      // state.error = action.error.message
      state.error = "a";
      // console.log(action.payload)
    });

    builder.addCase(refreshTokens.fulfilled, (state, action) => {
      // const dispatch = useDispatch();

      const data = action.payload;
      console.log(data)
      // Save the tokens in the state
      state.acessToken = data.access;
      // dispatch(getUserData())
      
      });
    builder.addCase(refreshTokens.rejected, (state, action) => {
      // state.error = action.error.message
      state.error = "c";
      // console.log(action.payload)
    });
  },
});

// Action creators are generated for each case reducer function
export { getTokens,refreshTokens };
export const { logout } = loginDataSlice.actions
export default loginDataSlice.reducer;
