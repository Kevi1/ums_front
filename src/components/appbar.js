import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import ImageAvatars from './avatarCircle';
import styles from './appbar.module.css'
import TemporaryDrawer from './drawer';
import { Drawer } from '@mui/material';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import { useRouter } from 'next/router';

const list = (router) => (

  <Box
   // sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
    // role="presentation"
    // onClick={toggleDrawer(anchor, false)}
    // onKeyDown={toggleDrawer(anchor, false)}
  >
    <List>
      {['Subject', 'Grades', 'Degree', 'User', 'Faculty'].map((text, index) => (
        <ListItem key={text} disablePadding>
          <ListItemButton onClick={() =>{
            router.push(text.toLowerCase())
          }}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItemButton >
        </ListItem>
      ))}
    </List>
    <Divider />
  </Box>
);
const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  //backgroundColor: alpha(theme.palette.common.white, 0.15),
  //'&:hover': {
  //  backgroundColor: alpha(theme.palette.common.white, 0.5),
  //},
  backgroundColor: '#59b300',
  border: '#66cc00',
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

export default function SearchAppBar() {
  const router=useRouter()
  const [state, setState] = React.useState(false);
  return (
    <Box>
    <Box sx={{ backgroundColor:'#336600', flexGrow: 1 }}>
      <AppBar sx={{ backgroundColor:'#336600'}}position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="New Times Roman"
            sx={{ mr: 2 }}
            onClick={() =>{
              setState(true)
            }}
          >
            <MenuIcon />
          </IconButton>
          <Typography onClick={()=>{
            router.push('/')
          }}
            variant="h6"
            noWrap
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
          >
            University of Applied of Sciences
          </Typography >
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ 'aria-label': 'search' }}
            />
          </Search>
          <div>
            <ImageAvatars className={styles.avatar}/>
          </div>

          <Drawer
            anchor='left'
            open={state}
            onClose={() => {
              setState(false)
            }
            }
          >
            {list(router)}
          </Drawer>
        </Toolbar>
      </AppBar>
      </Box>
    </Box>
  );
}