import { useRouter } from "next/router";
import { useSelector } from "react-redux"; 
import { useState,useEffect } from "react";

export default function CanAccess({children}) {
    const [isLoading, setIsLoading] = useState(true)
    const router = useRouter()
    const role = useSelector(state=>state.userData.role)
  
    useEffect(() => {
        const checkToken = async () => {
        if(!token){
            router.push('/login')
        } else {
        // Additional token validation can be done here
        setIsLoading(false)
      }
    }

    checkToken()
  }, [])

  if (isLoading) {
    return <></>
  }

  return <>{children}</>
}