import { useRouter } from "next/router";
import { useSelector } from "react-redux"; 
import { useState,useEffect } from "react";

export default function AuthGuard({children,role}) {
    const [isLoading, setIsLoading] = useState(true)
    const router = useRouter()
    const token = useSelector(state=>state.loginData.acessToken)
    const userRole = useSelector(state=>state.userData.role)
    console.log(userRole)
    useEffect(() => {
        const checkToken = async () => {
        if(!token){
            router.push('/login')
        } else {
          if(!userRole==role){
            router.push('/404')
          }
        // Additional token validation can be done here
        setIsLoading(false)
      }

    }

    checkToken()
  }, [])

  if (isLoading) {
    return <></>
  }

  return <>{children}</>
}