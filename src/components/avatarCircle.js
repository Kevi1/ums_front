import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';

export default function ImageAvatars(props) {
  return (
    <div style={props.style} className={props.className}>
    <Stack direction="row" spacing={2}>
      <Avatar alt="Remy Sharp" src="/static/images/avatar/3.jpg" sx ={{width: 35, height: 35}} />
    </Stack>
    </div>
  );
}