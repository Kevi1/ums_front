import React from "react";
import { Controller, useFormContext } from "react-hook-form";
import TextField from '@mui/material/TextField';
import env from "../public/envelope-regular.svg";
import InputAdornment from '@mui/material/InputAdornment';
// import { TfiEmail } from 'react-icons/fa';
export const FormInputText = ({ name, control, label , text_type="",Icon ,pattern}) => {

  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required:true,
        pattern:pattern
        // validate:{
      }}
      render={({
        field: { onChange,value  },
        fieldState: { error },
        formState,
      }) => (
        <TextField
          helperText={error ? error.message : null}
          size="small"
          error={!!error}
          onChange={onChange}
          value={value}
          label={label}
          variant="outlined"
          type={text_type}
          sx={{
            border:"none"
          }}
          InputProps={ Icon ? {
            startAdornment: (
              <InputAdornment position="start">
              
              {Icon}
              
              </InputAdornment>
            ),
          }:null}
        />
      )}
    />
  );
};